// CRUD Operations
/* 
   C - Create (Insert Document)
   R - Read/Retrieve (View Specific Document)
   U - Update (Edit specific document)
   D - Delete (Remove specific document)
*/

// SECTION Insert a documents (Create)

/* 
   -Syntax:
      - db.collectionName.insertOne({object});
      Comparison with javascript
         object.object.method({object});
*/

db.users.insertOne({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
    phone: "87654321",
    email: "janedoe@gmail.com",
  },
  courses: ["CSS", "JavaScript", "Phyton"],
  department: "none",
});

// Syntax:
//

db.rooms.insertOne({
  name: "single",
  accomodates: "2",
  price: "1000",
  description: "A simple room with basic necessities",
  rooms_available: 10,
  isAvaialble: false,
});

/* Insert many
   - Syntax 
      - db.collectionName.insertMany([{},{}])
*/

db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87654321",
      email: "stephenhawking@gmail.com",
    },
    courses: ["Phython", "React", "PHP"],
    department: "none",
  },
  {
    firstName: "Niel",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "87654321",
      email: "nielarmstrong@gmail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none",
  },
]);

db.rooms.insertMany([
  {
    name: "double",
    accomodates: "3",
    price: "2000",
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvaialble: false,
  },
  {
    name: "queen",
    accomodates: "4",
    price: "4000",
    description: "A room with a queen sized bed perfect for a simple gataway",
    rooms_available: 15,
    isAvaialble: false,
  },
]);

// Retrieve a document (Read)
/* 
  -Syntax:
    -db.collectName.find({});
    -db.collectName.find({field:value});

*/
// Find all the documents in the collection
db.users.find({});
// Find a specific documents in the collection using the field value.
db.users.find({ firstName: "Stephen" });

// Find documents with multiple parameters.
/* 
  -Syntax:
    - db.collectionName.find({fieldA:valueA, fieldB:valueB})
*/

db.users.find({ lastName: "Armstrong", age: 76 });

// SECTION updating documents (Update)

// Create document to update

db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "00000000",
    email: "test@gmail.com",
  },
  course: [],
  department: "none",
});
/* 
- Just like the find method, methods that only manipulate a single document will only the FIRST document that matches the search criteria.
  -Syntax:
  // criteria - specific document
    - db.collectionName.updateOne({criteria}, {$set:{field:value}});
*/
db.users.updateOne(
  { firstName: "Test" },
  {
    $set: {
      firstName: "Bill",
      lastName: "Gates",
      age: 65,
      contact: {
        phone: "12345678",
        email: "bill@gmail.com",
      },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations",
      status: "active",
    },
  }
);
// Updating multiple documents
/* 
  - Syntax
  - db.collectionName.updateMany({criteria}, {$set: {field:value}})
*/
db.users.updateMany(
  { department: "none" },
  {
    $set: {
      department: "HR",
    },
  }
);
ObjectId("630737179a149053771c2c68");

db.users.updateOne(
  {
    _id: ObjectId("630737179a149053771c2c68"),
  },
  {
    $set: {
      firstName: "Joar",
      lastName: "Penaflorida",
    },
  }
);

// Replace One

/* 
  -Syntax:
  - db.collectionName.replaceOne({criteria}, {$set:{field:value}})
*/

db.user.replaceOne(
  { firstName: "Bill" },
  {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
      phone: "12345678",
      email: "bill@gmail.com",
    },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations",
  }
);

db.rooms.updateOne({ name: "queen" }, { $set: { rooms_available: 0 } });

// Updating a document
// Check if the document is updated

db.rooms.find({ name: "queen" });

// SECTION Removing documents [DELETE]

// Deleting a single document

/* 
  - Syntax:
   -db.collectionName.deleteOne({criteria});
   Deletes the first document found.

*/
db.users.deleteOne({
  firstName: "Test",
});

// Delete Many
/* 
  - Be careful when using "deleteMany" method. If no search criteria is provided, it will delete all documents in the collection.
  
  -Syntax:
    - db.collectionName.deleteMany({criteria});
    - db.collectionName.deleteMany({});
*/
db.users.deleteMany({});

db.users.deleteMany({
  firstName: "Test",
});

db.rooms.deleteMany({
  rooms_available: 10,
});

db.rooms.insertMany([
  {
    _id: ObjectId("630810a66b68804447c9345f"),
    name: "single",
    accomodates: "2",
    price: "1000",
    description: "A simple room with basic necessities",
    rooms_available: 10.0,
    isAvaialble: false,
  },

  /* 2 */
  {
    _id: ObjectId("630817a4dc08bff5ef450936"),
    name: "double",
    accomodates: "3",
    price: "2000",
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5.0,
    isAvaialble: false,
  },
]);

// Retrieving through objects
//dot notation also works
db.users.find({
  courses: ["PHP", "Laravel", "HTML"],
});
// Quering an array with Exact Element

// Querying an array diregarding the array elements order.

db.users.find({
  courses: { $all: ["[a-c]%"] },
});
// Query on nested field
db.users.insertOne({
  nameArr: [
    {
      nameA: "Juan",
    },
    {
      nameB: "Tamad",
    },
  ],
});

db.users.find({
  nameArr: {
    nameA: "Juan",
  },
});
